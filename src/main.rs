// Modules
mod board;

// Namespaces
use std::*;

// Error enum
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Board: {0}")]
    BoardError(#[from] board::Error),
    #[error("No such command {0}")]
    NoSuchCommand(String),
    #[error("Parse: {0}")]
    ParseError(#[from] num::ParseIntError),
    #[error("Readline: {0}")]
    ReadlineError(#[from] rustyline::error::ReadlineError),
    #[error("Regex: {0}")]
    RegexError(#[from] regex::Error),
}

// Result type
pub type Result<T = ()> = result::Result<T, Error>;

// Main function
pub fn main() -> Result {
    let mut board = board::Board::new_empty();
    let mut debug = false;
    let mut rl = rustyline::DefaultEditor::with_config(
        rustyline::Config::builder()
            .history_ignore_space(true)
            .auto_add_history(true)
            .tab_stop(4)
            .indent_size(4)
            .build(),
    )?;
    println!("Enter 'help' for usage");
    while let Ok(line) = rl.readline("> ") {
        if let Err(err) = (|| {
            if regex::Regex::new(r"^help")?.is_match(&line) {
                indoc::printdoc! {"
                    Usage:
                        help
                        debug
                        set <x> <y> <value>
                        solve
                        new
                "}
            } else if regex::Regex::new(r"^debug")?.is_match(&line) {
                debug = !debug;
                if debug {
                    print!("{board:?}");
                } else {
                    print!("{board}");
                }
            } else if let Some(caps) =
                regex::Regex::new(r"^set (?<x>\S+) (?<y>\S+) (?<value>\S+)")?.captures(&line)
            {
                let x = caps.name("x").map_or("", |m| m.as_str()).parse()?;
                let y = caps.name("y").map_or("", |m| m.as_str()).parse()?;
                let value = caps.name("value").map_or("", |m| m.as_str()).parse()?;
                board.set(x, y, value)?;
                if debug {
                    print!("{board:?}");
                } else {
                    print!("{board}");
                }
            } else if regex::Regex::new(r"^solve")?.is_match(&line) {
                board = board.solve()?;
                print!("{board}");
            } else if regex::Regex::new(r"^new")?.is_match(&line) {
                board = board::Board::new_empty();
                if debug {
                    print!("{board:?}");
                } else {
                    print!("{board}");
                }
            } else {
                return Err(Error::NoSuchCommand(line));
            }
            Ok(())
        })() {
            println!("{err}");
        }
        println!();
    }
    Ok(())
}
