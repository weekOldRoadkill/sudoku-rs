// Namespaces
use rand::prelude::*;
use std::*;

// Error enum
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Cell value conflict at {0} {1}")]
    CellValueConflict(usize, usize),
    #[error("Impossible value {0}")]
    ImpossibleValue(usize),
    #[error("Out of bounds position {0} {1}")]
    OutOfBoundsPosition(usize, usize),
    #[error("Out of bounds value {0}")]
    OutOfBoundsValue(usize),
    #[error("Unable to solve")]
    UnableToSolve,
}

// Result type
pub type Result<T = ()> = result::Result<T, Error>;

// Cell enum
#[derive(Clone)]
enum Cell {
    Empty(collections::BTreeSet<usize>),
    Filled(usize),
}

// Cell functions
impl Cell {
    // Create a new empty cell
    fn new_empty() -> Self {
        Self::Empty((1..=9).into_iter().collect())
    }

    // Create a new filled cell
    fn new_filled(value: usize) -> Result<Self> {
        if value >= 1 && value <= 9 {
            Ok(Self::Filled(value))
        } else {
            Err(Error::OutOfBoundsValue(value))
        }
    }
}

// Debug trait implementation for Cell
impl fmt::Debug for Cell {
    fn fmt(self: &Self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Empty(possible_values) => {
                for value in 1..=9 {
                    if possible_values.contains(&value) {
                        write!(fmt, "{value}")?;
                    } else {
                        write!(fmt, " ")?;
                    }
                }
                Ok(())
            }
            Self::Filled(value) => write!(fmt, "  - {value} -  "),
        }
    }
}

// Display trait implementation for Cell
impl fmt::Display for Cell {
    fn fmt(self: &Self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Empty(_possible_values) => write!(fmt, " "),
            Self::Filled(value) => write!(fmt, "{value}"),
        }
    }
}

// Board struct
#[derive(Clone)]
pub struct Board {
    cells: [[Cell; 9]; 9],
}

// Board functions
impl Board {
    // Create a new empty board
    pub fn new_empty() -> Self {
        Self {
            cells: array::from_fn(|_x| array::from_fn(|_y| Cell::new_empty())),
        }
    }

    // Mutable access to a cell
    fn get_mut(self: &mut Self, x: usize, y: usize) -> Result<&mut Cell> {
        if x < 9 && y < 9 {
            Ok(&mut self.cells[x][y])
        } else {
            Err(Error::OutOfBoundsPosition(x, y))
        }
    }

    // Set a cell value
    pub fn set(self: &mut Self, x: usize, y: usize, value: usize) -> Result {
        let cell = self.get_mut(x, y)?;
        match cell {
            Cell::Empty(possible_values) => {
                if possible_values.contains(&value) {
                    *cell = Cell::new_filled(value)?;
                    for x in 0..9 {
                        if let Cell::Empty(possible_values) = self.get_mut(x, y)? {
                            possible_values.remove(&value);
                        }
                    }
                    for y in 0..9 {
                        if let Cell::Empty(possible_values) = self.get_mut(x, y)? {
                            possible_values.remove(&value);
                        }
                    }
                    let (x, y) = (x / 3 * 3, y / 3 * 3);
                    for x in x..x + 3 {
                        for y in y..y + 3 {
                            if let Cell::Empty(possible_values) = self.get_mut(x, y)? {
                                possible_values.remove(&value);
                            }
                        }
                    }
                    Ok(())
                } else {
                    Err(Error::ImpossibleValue(value))
                }
            }
            Cell::Filled(_value) => Err(Error::CellValueConflict(x, y)),
        }
    }

    // Check if the board is done
    fn is_done(self: &Self) -> bool {
        self.cells.iter().flatten().all(|cell| match cell {
            Cell::Empty(_possible_values) => false,
            Cell::Filled(_value) => true,
        })
    }

    // Try to solve the board with a specific value at a specific position
    fn try_solve_at(self: &Self, x: usize, y: usize, value: usize) -> Result<Self> {
        let mut board = self.clone();
        board.set(x, y, value)?;
        if board.is_done() {
            Ok(board)
        } else {
            board.try_solve()
        }
    }

    // Get the priority of the cells
    fn priority(self: &Self) -> Vec<(usize, usize, usize)> {
        let mut priority: Vec<_> = (0..9)
            .into_iter()
            .map(|x| (0..9).into_iter().map(move |y| (x, y)))
            .flatten()
            .filter_map(|(x, y)| match &self.cells[x][y] {
                Cell::Empty(possible_values) => Some((x, y, possible_values)),
                Cell::Filled(_value) => None,
            })
            .collect();
        priority.shuffle(&mut thread_rng());
        match priority
            .iter()
            .min_by_key(|(_x, _y, possible_values)| possible_values.len())
        {
            Some((x, y, possible_values)) => possible_values
                .iter()
                .map(|value| (*x, *y, *value))
                .collect(),
            None => Vec::new(),
        }
    }

    // Tries to solve the board
    fn try_solve(self: &Self) -> Result<Self> {
        self.priority()
            .into_iter()
            .map(|(x, y, value)| self.try_solve_at(x, y, value))
            .find(result::Result::is_ok)
            .ok_or(Error::UnableToSolve)?
    }

    // Try to solve the board in a certain number of steps
    pub fn solve(self: &Self) -> Result<Self> {
        if self.is_done() {
            Ok(self.clone())
        } else {
            (0..100)
                .into_iter()
                .map(|_i| self.try_solve())
                .find(result::Result::is_ok)
                .ok_or(Error::UnableToSolve)?
        }
    }
}

// Debug trait implementation for Board
impl fmt::Debug for Board {
    fn fmt(self: &Self, fmt: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..9 {
            for x in 0..9 {
                write!(fmt, "{:?} ", self.cells[x][y])?;
                if x % 3 == 2 && x < 8 {
                    write!(fmt, "| ")?;
                }
            }
            writeln!(fmt)?;
            if y % 3 == 2 && y < 8 {
                writeln!(
                    fmt,
                    "------------------------------|-------------------------------|------------------------------"
                )?;
            }
        }
        Ok(())
    }
}

// Display trait implementation for Board
impl fmt::Display for Board {
    fn fmt(self: &Self, fmt: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..9 {
            for x in 0..9 {
                write!(fmt, "{} ", self.cells[x][y])?;
                if x % 3 == 2 && x < 8 {
                    write!(fmt, "| ")?;
                }
            }
            writeln!(fmt)?;
            if y % 3 == 2 && y < 8 {
                writeln!(fmt, "------|-------|------")?;
            }
        }
        Ok(())
    }
}
